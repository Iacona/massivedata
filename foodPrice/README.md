# Visualisation de relations


Les graphiques de relations permettent d'explorer les rapports qu'entretiennent différentes valeurs entre elles. Un graphique de relation vous permet de trouver des corrélations entre données, des valeurs hors norme et des groupes de données.

Les graphiques de relation peuvent vous aider à répondre à ce genre de question :

- Existe-t-il une corrélation entre les dépenses publicitaires et les ventes de nos produits ?

- De quelle façon les dépenses et les recettes varient-elles par région ?

ici nous avons utiliser un **nuage de point** :

nous examinons le prix de certain aliment en Nouvelle Zeland entre 2006 et 2020.

Nous avons **22 709** elements.

## all_data

dans le dossier *all_data* vous trouverez un graphe presentant tout les aliments melangés selon leur prix en fonction du temps.

## banana_versus_orange

dans le dossier banana_versus_orange vous trouverez deux graphe presentant la variation du prix du kilos de banane (en jaune) et celui des oranges (en orange) en fonction de la data.
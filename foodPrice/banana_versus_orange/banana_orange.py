import pandas as pd
import matplotlib.pyplot as plt
import datetime

df = pd.read_csv("food-price-index-dec19-weighted-average-prices-csv-tables.csv")

orange_price = list()
banana_price = list()
date = list()

for index, row in df.iterrows():
    if (row['Series_title_1'] == "Oranges, 1kg"):
        orange_price.append(row['Data_value'])
    elif (row['Series_title_1'] == "Bananas, 1kg"):
        banana_price.append(row['Data_value'])
        date.append(datetime.datetime(int(row['Period']), int((round(row['Period'] % 1, 2) * 100)), 1))

plt.scatter(date, orange_price, color='#ff9933')
plt.scatter(date, banana_price, color='#ffff66')
plt.show()


plt.plot(date, orange_price, color='#ff9933')
plt.plot(date, banana_price, color='#ffff66')
plt.show()

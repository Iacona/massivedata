import pandas as pd
import matplotlib.pyplot as plt
import datetime

df = pd.read_csv("food-price-index-dec19-weighted-average-prices-csv-tables.csv")

new_date = list()
for index, row in df.iterrows():
    new_date.append(datetime.datetime(int(row['Period']), int((round(row['Period'] % 1, 2) * 100)), 1))

y = df['Data_value']

plt.xlabel('Temps')
plt.ylabel('Prix')

plt.scatter(new_date, y)
plt.show()
import matplotlib.pyplot as plt
import pandas as pd
import datetime
from matplotlib.dates import date2num

df = pd.read_csv("food-price-index-dec19-weighted-average-prices-csv-tables.csv")

aliment = []
nom_aliment = list()
date = list()
idx = -1
idx_tmp = 0
tmp = ""

for index, row in df.iterrows():
    if (row['Series_title_1'] != tmp and row['Series_title_1'] != ""):
        tmp = row['Series_title_1']
        new_list = []
        idx += 1
        idx_tmp += 1
        aliment.append(new_list)
        nom_aliment.append(row['Series_title_1'])
    if (row['Series_title_1'] == tmp):
        aliment[idx].append(row['Data_value'])
        if (idx_tmp == 1):
            date.append(datetime.datetime(int(row['Period']), int((round(row['Period'] % 1, 2) * 100)), 1))

ax = plt.subplot(111)
w = 15
date = date2num(date)
toto = [x + w for x in date]
tata = [x + w for x in date]
ax.bar(toto, aliment[0], width=w, color='b', align='center')
ax.bar(date, aliment[1], width=w, color='g', align='center')
ax.bar(tata, aliment[2], width=w, color='r', align='center')
ax.xaxis_date()
ax.autoscale(tight=True)
plt.show()